<?php

return [
    'create' => 'Створити',
    'edit'   => 'Редагувати',
    'delete' => 'Видалити',
    "update" => "Оновити",
    "save"   =>  "Зберегти",
    "search" =>  "Пошук",
    "no_items_found" => "Елементів не знайдено.",
    "action" => "Дія",
    "submit" => "Створити",
    "drop_files_here" => "Перетягніть файли сюди",
    "no" => "Ні",
    "yes" => "Так",
    "please_confirm" => "Будь-ласка підтвердіть",
    "expand" => "Розгорнути",
    "collapse" => "Згорнути",
    "pagination" => [
        "Next &raquo;" => "Вперед &raquo;",
        "&laquo; Previous" => "&laquo; Назад",
        "..." => "..."
    ],
    "menu" => [
        "dashboard" => "Панель",
        "management" => "Керування користувачами",
            "users" => "Користувачі",
            "roles" => "Ролі",
            "permissions" => "Дозволи",
        "catalog" => "Каталог",
            "categories" => "Категорії",
        "settings" => "Налаштування",
        "messages" => "Сповіщення",
        "languages" => "Мови",
        "Logout" => "Вихід"
    ]
];
