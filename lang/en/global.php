<?php

return [
    "create" => "Create",
    "edit"   => "Edit",
    "delete" => "Delete",
    "update" => "Update",
    "save"   =>  "Save",
    'search' =>  'Search',
    "no_items_found" => "No items found.",
    "action" => "Action",
    "submit" => "Submit",
    "drop_files_here" => "Drop files here",
    "no" => "No",
    "yes" => "Yes",
    "please_confirm" => "Please confirm",
    "expand" => "Expand",
    "collapse" => "Collapse",
    "pagination" => [
        "Next &raquo;" => "Next &raquo;",
        "&laquo; Previous" => "&laquo; Previous",
        "..." => "..."
    ],
    "menu" => [
        "dashboard" => "Dashboard",
        "management" => "Management",
        "users" => "Users",
        "roles" => "Roles",
        "permissions" => "Permissions",
        "catalog" => "Catalog",
        "categories" => "Categories",
        "settings" => "Settings",
        "messages" => "Messages",
        "languages" => "Languages",
        "Logout" => "Logout"
    ]
];
