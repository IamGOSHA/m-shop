<?php
return [
    "title" => "Category",
    "title_plural" => "Categories",
    "title_create" => "Category create",
    "title_edit"   => "Category edit",
    "no_parents" => "No parents",
    "root" => "Root",
    "table_fields" => [
        "id"   => "Id",
        "title" => "Title",
        "active" => "Active",
        "description" => "Description",
        "created_at" => "Created at",
        "updated_at" => "Updated at",
    ],
    "fields" => [
        "title" => "Title",
        "parent" => "Parent Category",
        "description" => "Description",
        "active" => "Displayed",
        "link_rewrite" => "Friendly URL",
        "meta_title"  => "Meta title",
        "meta_description"  => "Meta description",
        "meta_keywords"  => "Meta keywords",
        "cover_image" => "Category cover image",
        "menu_thumbnail" => "Menu thumbnail"
    ]
];
